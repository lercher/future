package future_test

import (
	"errors"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/lercher/future"
)

func TestThenResolved(t *testing.T) {
	v := future.Resolved(42, nil)
	n := 10000
	sum := 0

	for i := 0; i < n; i++ {
		v.Then(func(val int, err error) {
			if err != nil {
				t.Fatal(err)
			}
			sum += val
		})
	}

	if want := 42 * n; sum != want {
		t.Errorf("want %v, got %v", want, sum)
	}
}

func TestThenCallback(t *testing.T) {
	v, r := future.Create[int]()
	n := 10
	sum := 0

	for i := 0; i < n; i++ {
		// only the last one callback
		v.Then(func(val int, err error) {
			if err != nil {
				t.Fatal(err)
			}
			sum += val
		})
	}

	r(42, nil)
	if want := 42; sum != want {
		t.Errorf("want %v, got %v", want, sum)
	}
}

func TestThenNilCallback(t *testing.T) {
	v, r := future.Create[int]()
	n := 10
	sum := 0

	for i := 0; i < n; i++ {
		// only the last one callback
		v.Then(func(val int, err error) {
			if err != nil {
				t.Fatal(err)
			}
			sum += val
		})
	}
	v.Then(nil)

	r(42, nil)
	if want := 0; sum != want {
		t.Errorf("want %v, got %v", want, sum)
	}
}

func TestPanicOnDoubleResolve(t *testing.T) {
	defer func() {
		want := `resolver function called for an already resolved future Value`
		r := recover()
		if r != want {
			t.Errorf("want panic %q, got %q", want, r)
		}
	}()

	_, r := future.Create[int]()
	r(42, nil)
	r(48, errors.New("panic expected"))
	t.Error("this line should not be reached b/c of the panic")
}

func TestBlockingGets(t *testing.T) {
	v, r := future.Create[string]()
	var wg sync.WaitGroup
	var resolved atomic.Bool

	n := 100
	wg.Add(n)
	for i := 0; i < n; i++ {
		i := i
		go func() {
			defer wg.Done()
			if v.IsResolved() {
				t.Errorf("#%v IsResolved is unexpected before Get() -> time.After duration too small?", i)
			}
			if resolved.Load() {
				t.Errorf("#%v is resolved is unexpected before Get() -> time.After duration too small?", i)
			}
			val, err := v.Get()
			if !resolved.Load() {
				t.Errorf("#%v not resolved is unexpected after Get()", i)
			}
			if !v.IsResolved() {
				t.Errorf("#%v not IsResolved is unexpected after Get()", i)
			}
			if err != nil {
				t.Errorf("#%v %v", i, err)
				return
			}
			if val != "no-future" {
				t.Errorf("#%v want no-future, got %v", i, val)
			}
		}()
	}

	t.Log("no-future in 100ms")
	<-time.After(100 * time.Millisecond)

	t.Log("no-future is now")
	resolved.Store(true)
	r("no-future", nil)

	wg.Wait()
	t.Log("if this was logged, all Get()s succeeded")

	val, err := v.Get()
	if err != nil {
		t.Errorf("#after-all %v", err)
		return
	}
	if val != "no-future" {
		t.Errorf("#after-all want no-future, got %v", val)
	}
}

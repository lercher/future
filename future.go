// Package future provides access to a value
// that is known not until some external trigger
// provides the value or an error
package future

// Create returns a new Future and a function
// that needs to be called when the value or error
// for the future is known.
//
// The Resolver function must be called at most once.
// It panics otherwise.
func Create[T any]() (*Value[T], Resolver[T]) {
	f := &Value[T]{}
	f.muValue.Lock() // block Get()s and store Then() funcs
	return f, f.resolve
}

// Resolved creates an already resolved future.
func Resolved[T any](val T, err error) *Value[T] {
	f := &Value[T]{
		val: val,
		err: err,
	}
	f.resolved.Store(true)
	return f
}

// Transform is the sync version of Compose:
// A future T1-Value is transformed into a future T2-Value by a func T1 -> T2.
//
// Transform immediately calls f after v1 was resolved sucessfully and
// resolves the returned T2-Value.
// If v1 resolved to an error, so is the returned T2-Value and f is not called.
func Transform[T1, T2 any](v1 *Value[T1], f func(T1) (T2, error)) *Value[T2] {
	return Compose(v1, func(val1 T1, resolve Resolver[T2]) {
		resolve(f(val1))
	})
}

// Compose composes the resolution of a given T1-Value with a resolution of
// the returned T2-Value by the function f converting a T1 to a T2 into a
// T2-Resolver.
//
// It works this way:
//   - a new T2 future Value v2 is created which is returned, finally
//   - when the T1 future Value v1 is resolved with an error, this error resolves v2 immediatly
//   - when v1 is resolved normally, f is called with this T1 and the resolver of v2
//   - it is the responsibility of f to finally call the passed T2-resolver, either sync or async
//   - this last resolver call for v2 triggers v2's normal "I'm resolved now" behavior
func Compose[T1, T2 any](v1 *Value[T1], f func(val T1, resolve Resolver[T2])) *Value[T2] {
	v2, r2 := Create[T2]()
	v1.Then(func(val1 T1, err1 error) {
		switch err1 {
		case nil:
			f(val1, r2)

		default: // err != nil
			var zero2 T2
			r2(zero2, err1)
		}
	})
	return v2
	// false positive for nilaway
	//		C:\git\src\gitlab.com\lercher\future\future_test.go:143:16: error: Potential nil panic detected. Observed nil flow from source to dereference point:
	//	       -> C:\git\src\gitlab.com\lercher\future\future.go:62:9: unassigned variable `v2` returned from `Compose()` in position 0
	//	       -> C:\git\src\gitlab.com\lercher\future\future_test.go:143:16: result 0 of `Compose()` assigned deeply into local variable `futures`
}

package future

// Resolver is used to tell a future Value about
// its actual value or error.
// Use Create() to obtain a Future and its Resolver.
//
// Resolver is also the type of function to be passed
// to a future Value's Then method for being notified
// about a resolution of the future Value.
type Resolver[T any] func(val T, err error)

package future_test

import (
	"errors"
	"strconv"
	"sync"
	"sync/atomic"
	"testing"

	"gitlab.com/lercher/future"
)

func TestResolved(t *testing.T) {
	v42, want := future.Resolved(42, nil), 42
	if got, err := v42.Get(); got != want || err != nil {
		t.Errorf("value want %v, got %v", want, got)
	}

	ferr, want, werr := future.Resolved(42, errors.New("error 48")), 42, errors.New("error 48")
	if got, err := ferr.Get(); got != want || err.Error() != werr.Error() {
		t.Errorf("value want %v, got %v", want, got)
		t.Errorf("error want %v, got %v", werr, err)
	}
}

func TestCreate(t *testing.T) {
	v42, r42 := future.Create[string]()
	want := "42"
	if v42.IsResolved() {
		t.Errorf("IsResolved want %v, got %v", false, true)
	}
	r42(want, nil)
	if got, err := v42.Get(); got != want || err != nil {
		t.Errorf("value want %v, got %v", want, got)
	}
}

func TestTransformAndCompose(t *testing.T) {
	vs, r := future.Create[string]()
	vi := future.Transform(vs, strconv.Atoi)

	r("42", nil)

	// verify intermediate result
	vs.Then(func(val string, err error) {
		if val != "42" {
			t.Errorf("want %q, got %q", "42", val)
		}
		i, _ := strconv.Atoi(val)
		if i != 42 {
			t.Errorf("want %v, got %v", 42, i)
		}
	})

	// verify end result which should be "42".Atoi
	want := 42
	got, err := vi.Get()
	if err != nil {
		t.Fatal(err)
	}
	if want != got {
		t.Errorf("want %v, got %v", want, got)
	}
}

func square(i int) (int, error) {
	return i * i, nil
}

func TestTransformErrorUpfront(t *testing.T) {
	v0 := future.Resolved("Uranium-238", nil)
	v1 := future.Transform(v0, strconv.Atoi)
	v2 := future.Transform(v1, square)

	_, err := v2.Get()
	if err == nil /* sic! */ {
		t.Fatalf("expected error, got %v", err)
	}

	want := `strconv.Atoi: parsing "Uranium-238": invalid syntax`
	if err.Error() != want {
		t.Errorf("want %v, got %v", want, err)
	}
}

func TestTransformErrorDelayed(t *testing.T) {
	v0, r0 := future.Create[string]()
	v1 := future.Transform(v0, strconv.Atoi)
	v2 := future.Transform(v1, square)

	r0("Uranium-238", nil)

	_, err := v2.Get()
	if err == nil /* sic! */ {
		t.Fatalf("expected error, got %v", err)
	}

	want := `strconv.Atoi: parsing "Uranium-238": invalid syntax`
	if err.Error() != want {
		t.Errorf("want %v, got %v", want, err)
	}
}

func TestTransform(t *testing.T) {
	v0, r0 := future.Create[string]()
	v1 := future.Transform(v0, strconv.Atoi)
	v2 := future.Transform(v1, square)

	r0("42", nil)

	got, err := v2.Get()
	if err != nil {
		t.Fatal(err)
	}

	want := 42 * 42
	if got != want {
		t.Errorf("want %v, got %v", want, got)
	}
}

func TestComposeChaos(t *testing.T) {
	item, n := 42, 100

	var wg sync.WaitGroup
	wg.Add(n)

	var sum atomic.Int64
	aggregate := func(i int, r future.Resolver[bool]) {
		sum.Add(int64(i))
		r(true, nil)
		wg.Done()
	}

	aggregateAsync := func(i int, r future.Resolver[bool]) {
		go aggregate(i, r)
	}

	futures := make([]*future.Value[bool], n)
	for i := 0; i < n; i++ {
		vi, r := future.Create[int]()
		vsi := future.Transform(vi, square)
		futures[i] = future.Compose(vsi, aggregateAsync)
		r(item, nil)
	}

	wg.Wait()

	want := int64(n * item * item)
	if got := sum.Load(); got != want {
		t.Errorf("want sum %v, got %v", want, got)
	}
}

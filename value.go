package future

import (
	"sync"
	"sync/atomic"
)

// Value governs the access to a value or an error
// that is known not until it as resolved.
type Value[T any] struct {
	mu       sync.Mutex  // mu guards the whole struct
	muValue  sync.Mutex  // muValue blocks until the value was resolved
	resolved atomic.Bool // it's atomic b/c as soon as it is true the value is r/o
	then     Resolver[T]
	err      error
	val      T
}

// IsResolved returns true after the future Value
// was resolved. It returns false if the Value is not
// yet known.
// It is guaranteed that Get() returns immediatly if
// IsResolved was true.
func (v *Value[T]) IsResolved() bool {
	return v.resolved.Load()
}

// Get retrieves the T value and the error of the Value.
// Get blocks infinitly until the value is resolved.
// It is an efficient call if the Value was resolved before.
func (v *Value[T]) Get() (T, error) {
	if v.IsResolved() {
		return v.val, v.err
	}

	v.mu.Lock()
	// v can be resolved after locking mu
	if v.IsResolved() {
		v.mu.Unlock()
		return v.val, v.err
	}
	v.mu.Unlock()

	// It's OK to lock muValue after mu.Unlock b/c if the
	// resolver gets executed at this very instant here, it releases the
	// muValue and we can lock it. If other Get()s are waiting on muValue
	// they will be granted muValue serially one ofter the other
	// which is a small but acceptable drawback b/c return v.val, v.err
	// plus muValue.Unlock is quick.
	// Very late Get()s will see v.IsResolved() == true and thus
	// return immediatly and concurrently.
	v.muValue.Lock()
	defer v.muValue.Unlock()

	return v.val, v.err
}

// Then calls or registers a func for later calling with the Value's T value and error.
//
// If the Value is positively not resolved b/c the Values's Resolver function was not yet called
// (don't rely on a blocked Get or a false IsResolved) Then() can be used to register
// a single function to be called during the Values's Resolver function call.
// Any subsequent call of Then replaces the registered callback func. This is
// normally used during initalization of a Value to be later notified about the
// resolution. We use the term "callback" for this mode of operation, and this
// is the recommended mode.
//
// After IsResolved() was true or after the first Get() call returned, Then() can be called
// many times with the same or with varying functions for immediatly calling them during Then().
//
// If Then() is called exactly once and the value is resolved exactly once
// then r is also called exactly once regardles of when Then() happens with respect
// to calling the resolver func.
//
// In detail:
//   - If the Value is not yet resolved, r is stored and called when the Value's resolver
//     func is called. r replaces any previously stored callback function.
//   - If the Value is already resolved, r is called immediatly.
//   - If r is nil, r is not called, but nil is stored,
//     effectivly removing a previously registered callback.
//   - There is no certainty weather r is called by a single Then()
//     call or by the resolver function call, if this Then() happens in parallel to
//     the resolution. However, r is not called twice in this situation. Just
//     don't do this.
func (v *Value[T]) Then(r Resolver[T]) {
	if v.IsResolved() {
		v.notify(r)
		return
	}

	v.mu.Lock()
	defer v.mu.Unlock()

	// v can be in resolved state after locking mu
	if v.IsResolved() {
		v.notify(r)
		return
	}

	v.then = r
	// mu.Unlock via defer
}

func (v *Value[T]) notify(r Resolver[T]) {
	if r != nil {
		r(v.val, v.err)
	}
}

func (v *Value[T]) resolve(val T, err error) {
	const alreadyResolved = "resolver function called for an already resolved future Value"

	if v.IsResolved() {
		panic(alreadyResolved)
	}

	v.mu.Lock()
	defer v.mu.Unlock()

	// v can be in resolved state after locking mu
	if v.IsResolved() {
		panic(alreadyResolved)
	}

	v.val = val
	v.err = err

	v.resolved.Store(true) // new Get() won't block any more now
	v.muValue.Unlock()     // release all previously pending Get()s
	v.notify(v.then)       // and call the stored Then Resolver func if present
	v.then = nil
	// mu.Unlock via defer
}

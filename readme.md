# future [![Go Reference](https://pkg.go.dev/badge/gitlab.com/lercher/future.svg)](https://pkg.go.dev/gitlab.com/lercher/future)

future is a Go package using generics governing access to a value
that is available not earlier than when it is resolved
by an external trigger.

## Usage

```sh
go get gitlab.com/lercher/future
```

### Intended Use Case

1. initialize the future value -> `Create`, `Resolved`
1. optionally compose with transformation functions -> `Compose`, `Transform`
1. optionally setup a callback -> `Then`
1. spawn a goroutine finally resolving the real value -> `Resolver`
1. do sth else
1. then optionally get the real value and block -> `Get`
1. _or_ just wait for the callback to happen -> `Resolver`
1. _or_ poll against the future value during "do sth else" -> `IsResolved`

Both the goroutine and the "do sth else" can be I/O bound or CPU bound.

### Creation API

```go
func Create[T any]() (*Value[T], Resolver[T])
// plain creation for a future Value (val, err) pair

func Resolved[T any](val T, err error) *Value[T]
// creating a pre-resolved "future" from (val err)

type Resolver[T any] func(val T, err error) 
// type of func returned by Create to resolve the future Value
```

### Composition API

Concept: Suppose you have a function from `T1` to `T2` and a future
`T1` value. Then by applying the function to the resolved value of the `T1` future
you get a future `T2` value.

We add error handling to this concept and we distinguish the async case (`Compose`)
from the sync case (`Transform`) in the concept's function.

```go
func Compose[T1, T2 any](v1 *Value[T1], f func(val T1, resolve Resolver[T2])) *Value[T2]
func Transform[T1, T2 any](v1 *Value[T1], f func(T1) (T2, error)) *Value[T2]
```

### Acquiring "Real" Values

```go
func (v *Value[T]) IsResolved() bool   // will Get return immediatly?
func (v *Value[T]) Get() (T, error)    // access the resolved (val, err) pair, blocks until resolved
func (v *Value[T]) Then(r Resolver[T]) // register a single callback on resolution
```

### Sample Code

A good idea is to look at the tests. The basic
use cases are something like these:

```go
import "gitlab.com/lercher/future" // et.al.

func get(t *testing.T) {
    v, r := future.Create[int]()
    // v.Get() would block here until r is called

    go func() {
        // some time goes by
        r(42, nil) // resolve the future value to be 42, nil error
    }()

    val, err := v.Get() // 42, nil     | blocks until r is called async
    val, err = v.Get()  // 42, nil too | doesn't block

    v.Then(func(val2, err2) {
        // val2, err2 == 42, nil
        // called during this v.Then() call
    })
    v.Then(func(val2, err2) {
        // val2, err2 == 42, nil
        // called during this v.Then() call
    })
}

func callback(t *testing.T) {
    v, r := future.Create[int]()
    v.Then(func(val, err) {
        // reached when r(42, nil) is called later
        // val, err == 42, nil
    })

    r(42, nil) // resolve later, calls the func literal above
}

func nocallback(t *testing.T) {
    v, r := future.Create[int]()
    v.Then(func(val, err) {
        // never called ...
    })
    v.Then(nil) // ... b/c of this

    r(42, nil) // resolve later
}

func nocallback2(t *testing.T) {
    v, r := future.Create[int]()
    v.Then(func(val, err) {
        // never called ...
    })
    v.Then(func(val, err) {
        // ... b/c of this func
        // but val, err == 42, nil is true here
    })

    r(42, nil) // resolve later
}

// don't code sth like this:
func panics(t *testing.T) {
    v := future.Resolved(42, nil) // immediatly resolved
    // v is *future.Value[int] b/c 42 is an int
    val, err := v.Get() // 42, nil

    go sth()

    if !v.IsResolved() {
        // *
        r(48, nil) // might or might not panic ...
        // ... b/c another Go routine might have resolved v at *
    }

    r(48, nil) // ***panics*** b/c this is a 2nd resolution!
}

func square(i int) (int, error) {
    return i * i, nil
}

func composition() {
    v0, r0 := future.Create[string]()
    v1 := future.Transform(v0, strconv.Atoi)
    v2 := future.Transform(v1, square)

    r0("42", nil)
    got, err := v2.Get() // 42*42 = "42" . Atoi . square
    ...
}
```

## License

This package is licensed under the [MIT license](LICENSE).

## Project status

This project is _incubating_, but it is not that complicated matter.
So, we expect to reach something useable very soon.
This implementation is based on mutexes and multithreading
is hard. _So, be aware, bugs might linger undetected._

### Code Coverage

Currently 90%, where the missing 10% are extra checks of the
internal atomic bool `resolved` after the mutex `mu` was
acquired. That's hard to test and very unlikely be needed if
using the package as intendend (see the so called section above).

```txt
go test -cover ./...
ok      gitlab.com/lercher/future       0.288s  coverage: 90.0% of statements
```

## Variants

Futures and promises can also be implemented easily using
Go channels. More often that not, problems addressed by
futures can be solved with Go channels alone. _It's worth
thinking about that before opting for futures implemented
by this library_.

Wikipedia mentions Coroutines and Channels and says:

> Futures can easily be implemented in channels: a future is a
> one-element channel, and a promise is a process that sends
> to the channel, fulfilling the future. This allows futures
> to be implemented in concurrent programming languages with
> support for channels, such as CSP and Go. The resulting futures
> are explicit, as they must be accessed by reading from the
> channel, rather than only evaluation.

Note, however, that a Go channel allows the retrieval of the
real value exactly once. With this package `Get()` succeeds forever.

## Ressources

- <https://en.wikipedia.org/wiki/Futures_and_promises>
- `go install go.uber.org/nilaway/cmd/nilaway@latest`
